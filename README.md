# Menu Searx

Originally published 29 Jan 2021

A simple program to search the [Searx](https://searx.me/) search engine through [Dmenu](https://tools.suckless.org/dmenu/) or [Rofi](https://github.com/davatorium/rofi/), sort through the results and open the selected URL in you prefered web browser. Works with almost any Searx instance and you can change which one to use easily.  
![Demo](images/demo.GIF)

# Prerequisites

The program requires [JQ](https://stedolan.github.io/jq/), a JSON processor, to work.  
The program requires either Dmenu or Rofi to work.

# Installation

If you wish to receive error notifications at anywhere but the terminal, see `Notifier` under `Config` on how to set a notification program.  
The program does not have a set Searx instance, check in `Searx Instance` under `Config` below to see how to set one.  
The default browser is set to [Surf](https://surf.suckless.org/), so see `Browser` under `Config` on how to change that. Make sure to pick an instance that will not severely rate limit you.

# Config

The entire script exists within `menusearx`, open it up to start configuring things.  

## Menu Program

Uncomment the line under `# assign menu prorgram` relating to your preferred menu application, or add your own.  
If you wish to change the colour of your menu program, that can be accomplished by inserting whatever flags are needed to the  `menu` variable, just before the `-p` tag.

## Notifier

The default program for sending important notifications is `notify-send` you can change this by changing the `notifier` variable.

## Searx Instance

You can set a Searx instance by changing the `instance` variable to the instance's domain name.

## Browser

You can set a Searx instance by changing the `browser` variable to the instance's domain name.

## Temporary Files Directory

If you do not wish to have temporary files be stored under `/tmp/menusearx`, you can change this directory by changing the `cachedir` variable to the desired directory.

## Search Cache

By default, the program with save the results of the last 10 searches you have made. If it finds that you have the results for a search already saved, it reads from those results rather than fetching them again. You may edit the number of searches save by changing the `cachenum` variable.
